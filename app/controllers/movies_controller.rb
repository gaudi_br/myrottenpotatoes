class MoviesController < ApplicationController
  def index
    @movies = Movie.all
  end

  def show
    id = params[:id]
    @movie = Movie.find(id)
  end

  def new
    # default: render 'new' template
  end

  def create
    debugger
    @movie = Movie.create!(movie_params)
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def movie_params
    params.require(:movie).permit(:title, :rating, :release_date, :description)
  end
end